/*
 * Copyright (C) 2021 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package outsideCommunication;

import coppelia.FloatWA;
import coppelia.IntW;
import coppelia.remoteApi;
import java.util.ArrayList;

/**
 *
 * @author leticia
 */
public class PositionRechargeArea {
    
    private remoteApi vrep;
    private int clientID;
    private int nAreas = 4;
    private IntW[] rechargeArea_handle;
    private ArrayList<FloatWA> rechargeAreaPositions;
    private int[] energyValue;
    
    private int maxCharge = 10;//Estação A e B ganham mais
    private int minCharge = 3;
    private int maxPunishment = 20;////Estação A e B perdem mais
    private int minPunishment = 10;
    
    //Diferenciar estações: Anotações do evernote
    
    public PositionRechargeArea (int clientID, remoteApi vrep) {
        this.vrep = vrep;
        this.clientID = clientID;
        rechargeArea_handle = new IntW[nAreas];
        rechargeAreaPositions = new ArrayList<>();
        setRechargeAreasPosition();
        
        energyValue = new int[]{maxCharge, maxCharge, minCharge, minCharge};
        
    }

    private void setRechargeAreasPosition() {
        //Get the position of recharge areas
        char areaName = 'A';
        for (int i = 0; i < nAreas; i++) {
            FloatWA position = new FloatWA(3);
          
            rechargeArea_handle[i] = new IntW(-1);

            String s = "Energy" + (areaName);
            areaName++;
            vrep.simxGetObjectHandle(clientID, s, rechargeArea_handle[i], remoteApi.simx_opmode_blocking);
            //System.out.println(position);
            vrep.simxGetObjectPosition(clientID, rechargeArea_handle[i].getValue(), -1, position, vrep.simx_opmode_blocking);
            
            rechargeAreaPositions.add(position);
        }
    }
    
    public ArrayList getRechargeAreasPosition(){
        return rechargeAreaPositions;
    }
    
    public int getEnergyValueAtPosition(int i){
        return energyValue[i];
    }

}
