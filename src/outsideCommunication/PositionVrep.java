package outsideCommunication;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import CommunicationInterface.SensorI;
import coppelia.FloatWA;
import coppelia.IntW;
import coppelia.remoteApi;

public class PositionVrep implements SensorI {
	
    private remoteApi vrep;
    private int clientID;
    private FloatWA position;
    private IntW handle;
    
    //private IntW leftWheel;
   // private IntW rightWheel;
   
    
    public PositionVrep (int clientID, IntW handle, remoteApi vrep) {
        this.handle = handle;

        this.vrep = vrep;
        this.clientID = clientID;
        this.position = new FloatWA(3);

    //this.leftWheel = leftWheel;
    //this.rightWheel = rightWheel;
    }

	@Override
	public Object getData() {
		FloatWA position = new FloatWA(3);
		vrep.simxGetObjectPosition(clientID, handle.getValue(), -1, position,
                vrep.simx_opmode_oneshot);
		//System.out.println("Position");

                return position;
	}
	
	public void resetData() {
		System.out.println("Resseting position");
		vrep.simxPauseCommunication(clientID, true);
                //vrep.simxStopSimulation(clientID, vrep.simx_opmode_blocking);
		FloatWA position = initFloatWA(false);
		vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript, "reset",  null , 
				null ,null , null , null, null , null, null, vrep.simx_opmode_blocking);
		vrep.simxSetObjectPosition(clientID, handle.getValue(), -1, position,
                vrep.simx_opmode_oneshot);
		FloatWA angles = initFloatWA(true);
		vrep.simxSetObjectOrientation(clientID, handle.getValue(), -1, angles, vrep.simx_opmode_oneshot);
		
               // clientID = vrep.simxStart("127.0.0.1", 25000, true, true, 5000, 5);
                vrep.simxPauseCommunication(clientID, false);
		vrep.simxSynchronousTrigger(clientID);

	}
	
	private FloatWA initFloatWA(boolean orient) {
		FloatWA position = new FloatWA(3);
		float[] pos = position.getArray();
		
		if (orient) {
			pos[0] = 0.0f;
			pos[1] = 0.0f;
			pos[2] = (float) Math.random() * 360;
		}
		else {
			pos[0] = (float) Math.random() * 0.5f;
			pos[1] = (float) Math.random() * 0.5f;
			pos[2] = 0.138f;
		}
		return position;
	}
	
	private void printToFile(FloatWA position,String filename) {
		try(FileWriter fw = new FileWriter(filename,true);
	            BufferedWriter bw = new BufferedWriter(fw);
	            PrintWriter out = new PrintWriter(bw))
	        {
	            out.println(position.getArray()[0] + " " + position.getArray()[1]);
	            out.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}

}

/*
sim.simxStopSimulation(clientID,sim.simx_opmode_blocking);
is_running = true;
while is_running
    error_code, ping_time = sim.simxGetPingTime(clientID);
    error_code, server_state = sim.simxGetInMessageInfo(clientID, sim.simx_headeroffset_server_state);
    is_running = bitand(server_state,1);
end
sim.simxStartSimulation(clientID,sim.simx_opmode_blocking);
*/