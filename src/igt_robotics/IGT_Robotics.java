package igt_robotics;

import outsideCommunication.OutsideCommunication;

import java.io.File;


public class IGT_Robotics {

    public static void main(String[] args) {
    	// removing previous .txt files
    	File folder = new File(".");
    	for (File f : folder.listFiles()) {
    		if(f.getName().endsWith(".txt") && !(f.getName().endsWith("QTable.txt"))) {
    			f.delete();
    		}
    	}
        OutsideCommunication oc = new OutsideCommunication();
        oc.start();
        AgentMind am = new AgentMind(oc, "learning");//exploring; learning 
    }
    
}
