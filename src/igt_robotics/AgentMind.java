package igt_robotics;
import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryContainer;
import br.unicamp.cst.core.entities.MemoryObject;
import br.unicamp.cst.core.entities.Mind;
import codelets.sensors.GetCuriosityCodelet;
import codelets.sensors.GetEnergyCodelet;
import codelets.sensors.GetGroundTruthCodelet;
import codelets.perceptuals.NeedExploreCodelet;
import codelets.perceptuals.NeedSurviveCodelet;
import codelets.learner.LearnerCodelet;
import codelets.behaviors.MoveBackwardsCodelet;
import codelets.behaviors.MoveDiagonalUpLeftCodelet;
import codelets.behaviors.MoveDiagonalUpRightCodelet;
import codelets.behaviors.StopCodelet;
import codelets.behaviors.MoveForwardsCodelet;
import codelets.behaviors.TurnLeftCodelet;
import codelets.behaviors.TurnRightCodelet;
import codelets.motor.MotorCodelet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import outsideCommunication.OutsideCommunication;
import coppelia.FloatWA;

public class AgentMind extends Mind {
    
    public static final int nivelDiscrNeeds = 10;
    
    public AgentMind(OutsideCommunication oc, String mode){
        
        super();
        
        //////////////////////////////////////////////
        //Declare Memory Objects
        //////////////////////////////////////////////
        
        /***********************Sensory MOs******************************/
        List groundT = Collections.synchronizedList(new ArrayList<ArrayList<FloatWA>>());
        MemoryObject groundTMO = createMemoryObject("GTMO",groundT); 
        
        int energy_data = 0;
        MemoryObject energyMO = createMemoryObject("ENERGY", energy_data);
        
        int curiosity_data = 0;
        MemoryObject curiosityMO = createMemoryObject("CURIOSITY", curiosity_data);
        
        /***********************Perceptual MOs******************************/
        int surviveLevel_data = 0;
        MemoryObject surviveLevelMO = createMemoryObject("SURVIVE", surviveLevel_data);
        
        int exploreLevel_data = 0;
        MemoryObject exploreLevelMO = createMemoryObject("EXPLORE", exploreLevel_data);
        
        /***********************Attentional MOs******************************/
        List actionsList = Collections.synchronizedList(new ArrayList<ArrayList>());
        MemoryObject actionsMO = createMemoryObject("ACTIONS",actionsList); 
        
        List statesList = Collections.synchronizedList(new ArrayList<String>());
        MemoryObject statesMO = createMemoryObject("STATES", statesList);
        
        /***********************Motor MOs******************************/ 
        //Container para Motor - Pois possuem mais de um comportamento
        MemoryContainer behaviorMO;
        behaviorMO = createMemoryContainer("BEHAVIOR");    
        

       ////////////////////////////////////////////
       //Codelets
       ////////////////////////////////////////////
       
        /***********************Sensorial Codelets******************************/
        Codelet getLocalizatin = new GetGroundTruthCodelet(oc.pioneer_orientation, oc.pioneer_position);
        getLocalizatin.addOutput(groundTMO);
        insertCodelet(getLocalizatin);
        
        Codelet getCuriosity = new GetCuriosityCodelet();
        getCuriosity.addOutput(curiosityMO);
        getCuriosity.addInput(groundTMO);
        insertCodelet(getCuriosity);
        
        Codelet getEnergy = new GetEnergyCodelet();
        getEnergy.addOutput(energyMO);
        getEnergy.addInput(groundTMO);
        insertCodelet(getEnergy);
        
        /***********************Perceptual Codelets******************************/
        Codelet needSurvive = new NeedSurviveCodelet();
        needSurvive.addInput(energyMO);
        //needSurvive.addInput(groundTMO);
        needSurvive.addOutput(surviveLevelMO);
        insertCodelet(needSurvive);
        
        Codelet needExplore = new NeedExploreCodelet();
        needExplore.addInput(curiosityMO);
        //needExplore.addInput(groundTMO);
        needExplore.addOutput(exploreLevelMO);
        insertCodelet(needExplore);
        
        /***********************Learner Codelets******************************/
        Codelet learner = new LearnerCodelet(oc, mode);
        learner.addInput(groundTMO);
        learner.addInput(surviveLevelMO);
        learner.addInput(exploreLevelMO);
        learner.addInput(actionsMO);
        learner.addInput(statesMO);
        learner.addInput(energyMO);
        learner.addOutput(actionsMO);
        learner.addOutput(statesMO);
        insertCodelet(learner);
        
        /***********************Behavioral Codelets******************************/
        Codelet moveF = new MoveForwardsCodelet();
        moveF.addInput(actionsMO);
        moveF.addOutput(behaviorMO);
        insertCodelet(moveF);
        
        Codelet moveB = new MoveBackwardsCodelet();
        moveB.addInput(actionsMO);
        moveB.addOutput(behaviorMO);
        insertCodelet(moveB);
        
        Codelet turnRight = new TurnRightCodelet();
        turnRight.addInput(actionsMO);
        turnRight.addOutput(behaviorMO);
        insertCodelet(turnRight);
        
        Codelet turnLeft = new TurnLeftCodelet();
        turnLeft.addInput(actionsMO);
        turnLeft.addOutput(behaviorMO);
        insertCodelet(turnLeft);
        
        Codelet diagonalUpLeft = new MoveDiagonalUpLeftCodelet();
        diagonalUpLeft.addInput(actionsMO);
        diagonalUpLeft.addOutput(behaviorMO);
        insertCodelet(diagonalUpLeft);
        
        Codelet diagonalUpRight = new MoveDiagonalUpRightCodelet();
        diagonalUpRight.addInput(actionsMO);
        diagonalUpRight.addOutput(behaviorMO);
        insertCodelet(diagonalUpRight);
        
        Codelet stop = new StopCodelet();
        stop.addInput(actionsMO);
        stop.addOutput(behaviorMO);
        insertCodelet(stop);
        
        /***********************Motor Codelets******************************/
        Codelet motors = new MotorCodelet(oc.right_motor, oc.left_motor);
        motors.addInput(behaviorMO);
        insertCodelet(motors);

        ///////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////
        
        // NOTE Sets the time interval between the readings
        // sets a time step for running the codelets to avoid heating too much your machine
        for (Codelet c : this.getCodeRack().getAllCodelets())
            c.setTimeStep(200);
	
        try {
            Thread.sleep(200);
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        
     
	// Start Cognitive Cycle
	start(); 
	
    }
}
