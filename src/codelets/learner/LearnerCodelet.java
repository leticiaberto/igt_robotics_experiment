/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codelets.learner;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import coppelia.FloatWA;
import br.unicamp.cst.learning.QLearning;
import outsideCommunication.OutsideCommunication;

/**
 *
 * @author leticia
 */
public class LearnerCodelet extends Codelet{
    
    private int time_graph;
    private static final float CRASH_TRESHOLD = 0.09f;

    private static final int MAX_ACTION_NUMBER = 700;

    private static final int MAX_EXPERIMENTS_NUMBER = 1000;
    private static final int LEVEL_NO_BATTERY = -110;

    private static final int NON_VALUE_NEEDS = 10000;
    
    private static final int totalNeedSurvive = 20; 
    private static final int totalNeedExplore = 20;
    private static final int totalX = 10;
    private static final int totalY = 10;
    private static final int totalGraus = 36;
            
    private QLearning ql;
    
    //ActionsList é um MO de Entrada e Saída, logo é necessário atualiza-lo para os demais codelets usarem
    private MemoryObject actionsListMO;
    
    /*Alterações Leticia*/
    private List gt = Collections.synchronizedList(new ArrayList<ArrayList<FloatWA>>());
    
    private List actionsWinnerList = new ArrayList<ArrayList>();
            
    private List<String> actionsList; 
    private List<String> statesList;
    
    private int needSurvive;
    private int needExplore;
    
    private OutsideCommunication oc;
    private int timeWindow;
    private int sensorDimension;
    
    private double global_reward;
    private double episodeReward;
    private int action_number;
    private int experiment_number;
    
    private String mode;
    private int n = 2;//Qntd de features de GroundTruth(gd) que serão armazenadas no MO
    private int energyRead;
    
    public LearnerCodelet (OutsideCommunication outc, String mode) {
        super();
        time_graph = 0;

        global_reward = 0;

        action_number = 0;

        experiment_number = 1;
        
        episodeReward = 0;
               
        //Itializing
        for(int i = 0; i < n; i++)
            gt.add(0);

        //Se mudar as strings de ações tem que mudar nos codelets de behavior também
        ArrayList<String> allActionsList  = new ArrayList<>(Arrays.asList("MoveBackwards", "MoveFowards", "TurnLeft", "TurnRight", "Stop", "DiagonalUpLeft", "DiagonalUpRight")); 

        // States are 0 1 2 ... 5^8-1. Just Saliency or just Sonar
        ArrayList<String> allStatesList = new ArrayList<>(Arrays.asList(IntStream.rangeClosed(0, totalNeedSurvive * totalNeedExplore * totalX * totalY * totalGraus).mapToObj(String::valueOf).toArray(String[]::new)));
    
       
        // QLearning initialization
        ql = new QLearning();
        ql.setActionsList(allActionsList);
        // learning mode ---> build q table from scratch
        if (mode.equals("learning")) {
        // Initialize QTable to 0
            for (int i=0; i < allStatesList.size(); i ++) {
                for (int j=0; j < allActionsList.size(); j++) {
                    ql.setQ(0, allStatesList.get(i), allActionsList.get(j));

                }
            }
        }
        // exploring mode ---> reloads q table 
        else {
            try {
                ql.recoverQ();
            }
            catch (Exception e) {
                System.out.println("ERROR LOADING QTABLE");
                System.exit(1);
            }
        }


        oc = outc;
        this.mode = mode;
    }

    @Override
    public void accessMemoryObjects() {

        MemoryObject MO;
        
        //Leticia
        MO = (MemoryObject) this.getInput("GTMO");
        gt = (List) MO.getI();
        
        MO = (MemoryObject) this.getInput("EXPLORE");
        needExplore = (int) MO.getI();
        
        MO = (MemoryObject) this.getInput("SURVIVE");
        needSurvive = (int) MO.getI();
        
        MO = (MemoryObject) this.getInput("ENERGY");
        energyRead = (int) MO.getI();
   
        MO = (MemoryObject) this.getOutput("STATES");
        statesList = (List) MO.getI();

        actionsListMO = (MemoryObject) this.getOutput("ACTIONS");
        //actionsList = (List) MO.getI();
    }

    @Override
    public void calculateActivation() {
            // TODO Auto-generated method stub

    }

    public static Object getLast(List list) {
            if (!list.isEmpty()) {
                    return list.get(list.size()-1);
            }
            return null;
    }

	
	
    @Override
    public void proc() {

        try {
            Thread.sleep(70);//Estava 90
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        
                
        String state = "-1";

        //valores aleatórios so pra garantir que esse codelet nao esteja mais rapido que o dos needs
        if (needExplore != NON_VALUE_NEEDS && needSurvive != NON_VALUE_NEEDS) {
            if (!actionsWinnerList.isEmpty() && mode.equals("learning")) {
                // Find reward of the current state, given previous  winner 
                check_stop_experiment(mode);
                
                Double reward = 0d;
                
                // Gets last action taken
                String lastAction = (String) ((ArrayList)actionsWinnerList.get(actionsWinnerList.size()-1)).get(0);
                // Gets last state that was in
                String lastState = statesList.get(statesList.size() - 1);
                
                //CALCULAR REWARD
                
                global_reward += reward;
                episodeReward += reward;
                
                //System.out.println("Global Reward: "+global_reward);
                //System.out.println("Episode Reward: "+episodeReward);
                
                // Updates QLearning table
                ql.update(lastState, lastAction, reward);
            }

            //Escolher de acordo com o modo a ser rodado o experimento
            state = getState();
            
            statesList.add(state);


            // Selects new best action to take
            //String actionToTake = ql.getAction(state);
            String actionToTake = "DiagonalUpRight";
        //"MoveBackwards", "MoveFowards", "TurnLeft", "TurnRight", "Stop", "DiagonalUpLeft", "DiagonalUpRight")); 
            action_number++;
            
            /*
                Contem a ação ATUAL vencedora e niveis dos needs
                actionWinnerActual(0) = ação escolhida
                actionWinnerActual(1) = nivel do need Explore(curiosidade)
                actionWinnerActual(2) = nivel do need Survive(Bateria)
            */
            ArrayList actionWinnerActual = new ArrayList();
            actionWinnerActual.add(actionToTake);        
            actionWinnerActual.add(needExplore);
            actionWinnerActual.add(needSurvive);
            
            //Ações e needs ao longo do tempo
            actionsWinnerList.add(actionWinnerActual);
            
            actionsListMO.setI(actionsWinnerList);
            
            System.out.println("Action LEARNER: "+actionToTake);
     
        }
        //System.out.println("actions: " + action_number);
        time_graph = printToFile(0, state, "states.txt", time_graph,0, true);
        
        if (mode.equals("exploring")) {
            if(!gt.isEmpty()){
                FloatWA positionGT = ((FloatWA) gt.get(0));
                printToFilePosition(positionGT, "positionsGroundTruthLearner.txt");
            }
            check_stop_experiment(mode);           
        }

    }
	
	
	
    public void check_stop_experiment(String mode) {

        boolean crashed = false;
        
        boolean noBattery = false;
        
        //Bateria mt desacarregada é estado terminal
        if(energyRead <= LEVEL_NO_BATTERY)
                noBattery = true;
        
        if (mode.equals("exploring") && (crashed || noBattery)) {
            //Test to see if prints the robots position closer to obstacle
            if(!gt.isEmpty()){
                FloatWA positionGT = ((FloatWA) gt.get(0));
                printToFilePosition(positionGT, "positionsGroundTruthLearner.txt");
            }
            printToFileActionsExploring("exploringActionsNumber.txt");
            //oc.stopSimulation();
            oc.finishSimulation();
            System.exit(0);
        }
        else if (mode.equals("learning") && (action_number >= MAX_ACTION_NUMBER || crashed || noBattery)) {
                System.out.println("Max number of actions or crashed");
                experiment_number = printToFile(episodeReward, global_reward, "rewards.txt", experiment_number, action_number,false);
                action_number = 0;
                episodeReward = 0;  
                System.out.println("Experiment Number: "+experiment_number);
                if (experiment_number > MAX_EXPERIMENTS_NUMBER) {
                        ql.storeQ();
                        //oc.stopSimulation();
                        oc.finishSimulation();
                        System.exit(0);
                }
                oc.pioneer_position.resetData();//reinicia com robô em outra posição
                
                
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                    Thread.currentThread().interrupt();
                }
        }
    }
	
    public String getState() {
        if (!gt.isEmpty()){
            float [] positionGT = ((FloatWA) gt.get(0)).getArray();
            float [] orientationGT = ((FloatWA) gt.get(1)).getArray();

            int PosX = (int) positionGT[0]/1; //Fica só com a parte inteira -- Discretização da posição X
            int PosY = (int) positionGT[1]/1; //Fica só com a parte inteira -- Discretização da posição Y
            int OrientZ = (int) orientationGT[2]/10; //Discretização da orientação Z

            //Deveria ser soma, nao é?
            int estado = needExplore * needSurvive * PosX * PosY * OrientZ;

            return Long.toString(estado);
        }
        return "0";
    }
  	
    private int printToFile(double episodeReward, Object object,String filename, int counter, int action_number, boolean check){

        if (!check || counter < MAX_EXPERIMENTS_NUMBER) {
            try(FileWriter fw = new FileWriter(filename,true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw))
            {
                if(filename.equals("rewards.txt"))
                    out.println(counter+" "+ action_number + " " + episodeReward + " " +object );
                else
                    out.println(counter+" "+object );

                out.close();
                return ++counter;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return counter;

    }
    
    private void printToFilePosition(FloatWA position,String filename) {
        try(FileWriter fw = new FileWriter(filename,true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(position.getArray()[0] + " " + position.getArray()[1]);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void printToFileActionsExploring(String filename) {
        try(FileWriter fw = new FileWriter(filename,true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
            {
                out.println(action_number);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
        		

}

