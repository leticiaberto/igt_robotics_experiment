/*
 * /*******************************************************************************
 *  * Copyright (c) 2012  DCA-FEEC-UNICAMP
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the GNU Lesser Public License v3
 *  * which accompanies this distribution, and is available at
 *  * http://www.gnu.org/licenses/lgpl.html
 *  * 
 *  * Contributors:
 *  *     K. Raizer, A. L. O. Paraense, R. R. Gudwin - initial API and implementation
 *  ******************************************************************************/
    
package codelets.motor;

/**
 *
 * @author leticia
 */

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import CommunicationInterface.MotorI;
import br.unicamp.cst.core.entities.MemoryContainer;
import org.json.JSONException;
import org.json.JSONObject;

public class MotorCodelet extends Codelet {
    
    private MemoryObject motorActionMO;
    private MemoryObject rm_speed_MO, lm_speed_MO; 
    private MotorI rm, lm;
    
    private MemoryContainer actionMO;
    
    private int MOVEMENT_TIME = 2000; // 2 seconds
    
    public MotorCodelet(MotorI rmo, MotorI lmo){
    	super();
        rm = rmo;
        lm = lmo;
    }

    @Override
    public void accessMemoryObjects() {       
        actionMO = (MemoryContainer)this.getInput("BEHAVIOR");
    }

    @Override
    public void calculateActivation() {

    }

    @Override
    public void proc() {
    	try {
            Thread.sleep(130);//Estava 100. O tempo esta em milisegundos (2000 = 2s)
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        System.out.println("Motor");
        //Utlizando a mensagem em JSON lida do Container para realizar a ação
        String comm = (String) actionMO.getI();
        if (comm == null) comm = "";
		
        if(!comm.equals("") ){

            try {
                JSONObject command = new JSONObject(comm);
                if (command.has("ACTION")) {
                    
                    String action = command.getString("ACTION");
                    System.out.println("Motor2");
                    double leftSpeed = command.getDouble("LeftMO");
                    double rightSpeed = command.getDouble("RightMO");
                    System.out.println("Action MOTOR: " + action); 
                    System.out.println("r: " + rightSpeed + " l: " + leftSpeed);

                    rm.setSpeed((float) rightSpeed);
                    lm.setSpeed((float) leftSpeed);
                }	
            } catch (JSONException e) {e.printStackTrace();}
        }
        
    }//fim proc
}
