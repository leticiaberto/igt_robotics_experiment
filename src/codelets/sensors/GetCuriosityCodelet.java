/*
 * Copyright (C) 2021 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codelets.sensors;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import coppelia.FloatWA;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author leticia
 */
public class GetCuriosityCodelet extends Codelet{
    
    //Contem o nivel atual do need. Inicialmente vamos ter 20 niveis
    private int nivelDiscrNeeds = 10;
    private int curiosity;
    private List localization = Collections.synchronizedList(new ArrayList<ArrayList<FloatWA>>());

    private MemoryObject curiosityMO;
    
    public GetCuriosityCodelet(){
        initLevelNeed();
    }
    
    @Override
    public void accessMemoryObjects() {
        MemoryObject MO = (MemoryObject) this.getInput("GTMO");
        localization = (List) MO.getI();
        
        curiosityMO = (MemoryObject) this.getOutput("CURIOSITY"); 
    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        curiosity++;
        
        //Para nao passar do maximo que setamos
        /*if(curiosity > nivelDiscrNeeds)
            curiosity = nivelDiscrNeeds;
        else if(curiosity < -nivelDiscrNeeds)
            curiosity = -nivelDiscrNeeds;
        */
        //System.out.println("Curiosity " + curiosity);
        //Atualiza valor no MO responsavel
        curiosityMO.setI(curiosity);
    }
    
    public void initLevelNeed(){
        //Inicializa needs com valores aleatorios
        Random value = new Random(42);
        
        curiosity = value.nextInt(nivelDiscrNeeds);//gera numeros entre 0 e o nivel desejado (90, por exemplo)
        boolean negPos = value.nextBoolean();//se for 1, gera um numero negativo, senao positivo
        if(negPos)
            curiosity = -curiosity;
    }
}   