/*
 * Copyright (C) 2021 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codelets.sensors;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import coppelia.FloatWA;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import outsideCommunication.OutsideCommunication;
/**
 *
 * @author leticia
 */
public class GetEnergyCodelet extends Codelet{
    
    //Contem o nivel atual do need. Inicialmente vamos ter 20 niveis
    private int nivelDiscrNeeds = 10;
    private int sizeStation = 2;
    private int energy;
    private List localization = Collections.synchronizedList(new ArrayList<ArrayList<FloatWA>>());
    
    private MemoryObject energyMO;
    
    //private OutsideCommunication oc;
    private OutsideCommunication allRechPos;//Contem o X1, Y1, X2, Y2 de cada estação de carregamento
    
    private ArrayList<FloatWA> rechargePos;
    //private ArrayList<ArrayList<FloatWA>> rechargePos = new ArrayList<>();
    
    public GetEnergyCodelet(){
        //Inicia sensor com algum valor aleatório
        initLevelNeed();
        //Pega posições das estações
        //rechargePos = allRechPos.rechargePositions.getRechargeAreasPosition();
    }
    

    @Override
    public void accessMemoryObjects() {      
        MemoryObject MO = (MemoryObject) this.getInput("GTMO");
        localization = (List) MO.getI();
        
        energyMO = (MemoryObject) this.getOutput("ENERGY");
    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        //a cada ciclo perde um pouco de energia
        energy--;
        
        //Para nao passar do maximo que setamos
        /*if(energy > nivelDiscrNeeds)
            energy = nivelDiscrNeeds;
        else if(energy < -nivelDiscrNeeds)
            energy = -nivelDiscrNeeds;
        */
        
        //se tiver na posição de alguma estação de energia, recarrega
        //checkIfAtStation();
        
        //System.out.println("Energy " + energy);
        //Atualiza valor no MO responsavel
        energyMO.setI(energy);
    }
    
    public void initLevelNeed(){
        //Inicializa needs com valores aleatorios
        Random value = new Random(42);
        
        energy = value.nextInt(nivelDiscrNeeds);//gera numeros entre 0 e o nivel desejado (90, por exemplo)
        boolean negPos = value.nextBoolean();//se for 1, gera um numero negativo, senao positivo
        if(negPos)
            energy = -energy;
    }
    
    //verifica se esta em alguma posição de carregamento
    public void checkIfAtStation(){
        float [] position = ((FloatWA) localization.get(0)).getArray();
        
        for (FloatWA rechargePo : rechargePos) {
            float xIni, yIni, xFim, yFim;
            /*rechargePro tem as coordenadas do centro do chão. Então subtraimos a metade do cumprimento
            para encontrar o x e y iniciais*/
            xIni = rechargePo.getArray()[0] - (sizeStation/2);
            xFim = rechargePo.getArray()[0] + (sizeStation/2);
            yIni = rechargePo.getArray()[1] - (sizeStation/2);
            yFim = rechargePo.getArray()[1] + (sizeStation/2);
            
            if((xIni <= position[0] && position[0] <= xFim) && (yIni <= position[1] && position[1] <= yFim)) {
                energy += 10;//arrumar valores
                break;
            } 
        }
    }

}
